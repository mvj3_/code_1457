[代码] [Java]代码
	/**
	 * 根据指定的图像路径和最小高/宽度来获取缩略图
	 * @param imagePath 图像的路径
	 * @param minLen 指定输出图像的最小高/宽度
	 * @return 生成的缩略图
	 */ 
	public static Bitmap getImageThumbnail(String imagePath, int minLen) { 
	    int width = 300;
	    int height = 300;
	    Bitmap bitmap = null; 
	    BitmapFactory.Options options = new BitmapFactory.Options();
	     
	    options.inJustDecodeBounds = true; 
	    bitmap = BitmapFactory.decodeFile(imagePath, options); 
	    options.inJustDecodeBounds = false;
	     
	    if(options.outHeight > options.outWidth) {
	        width = minLen;
	        options.inSampleSize = options.outWidth / width;
	        height = options.outHeight * width / options.outWidth;
	        options.outHeight = height;
	        options.outWidth = width;
	    }
	    else {
	        height = minLen;
	        options.inSampleSize = options.outHeight / height;
	        width = options.outWidth * height / options.outHeight;
	        options.outWidth = width;
	        options.outHeight = height;
	    }
	 
	    options.inPreferredConfig = Bitmap.Config.ARGB_4444;
	    options.inPurgeable = true;
	    options.inInputShareable = true;
	     
	    bitmap = BitmapFactory.decodeFile(imagePath, options); 
	    bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height, 
	            ThumbnailUtils.OPTIONS_RECYCLE_INPUT); 
	    return bitmap; 
	}